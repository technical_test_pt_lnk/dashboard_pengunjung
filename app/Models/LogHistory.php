<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class LogHistory extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'log_login_datas';
}