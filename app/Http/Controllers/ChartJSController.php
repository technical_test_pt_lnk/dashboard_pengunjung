<?php
  
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\LogHistory;
    
class ChartJSController extends Controller{

    public function index(){
        try{
            $labels = array();
            $data = array();
            $logs = LogHistory::get();

            foreach ($logs as $log) {
                if($log->hours != null){
                    array_push($labels, $log->username);
                    array_push($data, $log->hours);
                }
            }

        }catch(err){
            die('Error when get count pengunjung, '.err);
        }
        return view('welcome', compact('labels', 'data'));
    }
}