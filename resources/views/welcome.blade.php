<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="dist/img/logo.png" />
    <title>Dashboard Pengunjung | Technical Test PT LNK</title>
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>

<body class="hold-transition layout-top-nav">

    <div class="wrapper">
        <nav class="main-header navbar navbar-expand-md navbar-light elevation-3 slide-down"
            [style.background-color]="colorBackgroundHeader">
            <div class="container padding">
                <table width="100%">
                    <tr>
                        <td width="7%">
                            <img className="in-left" src="/dist/img/logo.png" style="width: 100%" />
                        </td>
                        <td>
                            <h2 class="in-left"><b>&nbsp;Dashboard Lama Pengunjung Kalkulator MERN</b></h2>
                        </td>
                    </tr>
                </table>
            </div>
        </nav>
        <div class="content-wrapper">
            <div class="content-header">
                <div class="container">
                </div>
            </div>
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-12">
                            <div class="card bordered in-left">
                                <div class="card-body bordered elevation-5">
                                    <div class="row">
                                        <div class="col-12">
                                            <canvas id="myChart" height="130px"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/chart.js"></script>
    <script src="dist/js/adminlte.min.js"></script>

    <script type="text/javascript">
    var labels = {{Js::from($labels)}};
    var users = {{Js::from($data)}};

    console.log(labels);
    console.log(users);
    const data = {
        labels: labels,
        datasets: [{
            label: 'lama (jam)',
            backgroundColor: '#27cfc3',
            borderColor: '#27cfc3',
            data: users,
        }]
    };

    const config = {
        type: 'line',
        data: data,
        options: {}
    };

    const myChart = new Chart(
        document.getElementById('myChart'),
        config
    );
    </script>
</body>

</html>